<?php 
/*
	Пример вставки согласующего пользователя (пользователькое поле, тип "привязка к сотруднику") из карточки сотрудника
	Блок: выполнение произвольного php кода

	{{Кем создан}} - автор
	UF_поле - код пользовательского поля
	переменная_пользователя_в_бизнес_процессах - переменная "пользователь" в бизнес-процессе
*/

$userId = substr("{{Кем создан}}", 5); // Пользователь - автор
$userId = intval($userId);

$arFilter = array("ID" =>$userId);
$arParams["SELECT"] = array("UF_поле"); 
$arRes = CUser::GetList($by,$desc,$arFilter,$arParams);
   if ($res = $arRes->Fetch()) {
       $rootActivity = $this->GetRootActivity();
       $rootActivity->SetVariable("переменная_пользователя_в_бизнес_процессах", "user_".$res["UF_поле"]);
   }
?>
